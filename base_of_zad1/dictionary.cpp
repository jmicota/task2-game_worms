#include <iostream>
#include <cctype>
#include <regex>
#include <string>
#include <algorithm>
#include <limits.h>

using namespace std;

extern "C" bool eol_is_carriage_return(char* line);
extern "C" bool starts_with_endl(char* line);
extern "C" bool search_double_endl(char* line);
extern "C" bool is_prefix(char* cat_path, char* full_path);
extern "C" bool single_slash(char* str_arg);
extern "C" int match_method(char* s_arg, bool* get_flag, bool* head_flag);
extern "C" int match_header_field(char* str_arg, bool* conn_cl_flag, 
                                  bool* cont_len_flag, bool* srv_flag);

/* All below to be freed after call. */
extern "C" char* build_prebody_message(size_t len_arg);
extern "C" char* build_location_message(char* name, char* ip, char* port);
extern "C" char* build_err_message(int st_code);
extern "C" char* connect_str(char* s1, char* s2);
extern "C" char* filepath_from_token(char* line);

#define SUCCESS             200
#define ERR_FMOVED          302
#define ERR_FORMAT          400
#define ERR_NFOUND          404
#define ERR_SRVSIDE         500
#define ERR_UNIMPL          501
#define CLOSE_CONNECTION    333
#define MESSAGE_OVER        334

#define TO_EXTRACT_FNAME_GET    14
#define TO_EXTRACT_FNAME_HEAD   15
#define MY_PATH_MAX         8195

string HFIELD_CONT_TYPE = "Content-Type: application/octet-stream";
string HFIELD_NAME_CONT_L = "Content-Length";
string HFIELD_NAME_LOC = "Location";
string HFIELD_SERVER = "Server: pozdrawiam_cieplutko";

// base:
string HTTPV = "HTTP/1.1";
string CRLF = "\r\n";
string SINGLE_SPACE = " ";
string COLON = ":";
string TAB = "\t";
string PROT = "http://";
string SLASH = "/";

// status codes strings:
string SUCCESS_STR = "200";
string ERR_FMOVED_STR = "302";
string ERR_FORMAT_STR = "400";
string ERR_NFOUND_STR = "404";
string ERR_SRVSIDE_STR = "500";
string ERR_UNIMPL_STR = "501";

// reason phrases:
string SUCCESS_DSC = "Success";
string ERR_FMOVED_DSC = "File moved";
string ERR_FORMAT_DSC = "Incorrect format";
string ERR_NFOUND_DSC = "Resource not found";
string ERR_SRVSIDE_DSC = "Error on server side";
string ERR_UNIMPL_DSC = "Unimplemented";

// regex matches (\n is a delim for strtok, so at the end looking for \r only):
regex GET_MATCH = regex("(GET /)([a-z0-9A-Z\-\\./]+)( HTTP/1.1\r)");
regex HEAD_MATCH = regex("(HEAD /)([a-z0-9A-Z\-\\./]+)( HTTP/1.1\r)");
regex HEADER_MATCH = regex("([a-zA-Z-]+):( )*([a-z0-9A-Z\-]+)( )*(\r)");
regex HEADER_CONN_CLOSE_MATCH = regex("(connection:)( )*(close)( )*(\r)");
regex HEADER_CONT_LEN_MATCH = regex("(content-length:)( )*([0-9]+)( )*(\r)");
regex HEADER_CONT_CORR_LEN_MATCH = regex("(content-length:)( )*([0]+)( )*(\r)");
regex HEADER_SERVER_MATCH = regex("(server:)( )*([0-9]+)( )*(\r)");
regex DOUBLE_ENDL_MATCH = regex("[a-zA-Z0-9\\.\-/ \r\n]*(\n\n)[a-zA-Z0-9\\.\-/ \r\n]*");
regex HEADER_EMPTY = regex("(\r)");
regex SLASH_MATCH = regex("(/)+");

string get_code_str(int status_code) {
    switch(status_code) {
        case SUCCESS:
            return SUCCESS_STR;
        case ERR_FMOVED:
            return ERR_FMOVED_STR;
        case ERR_FORMAT:
            return ERR_FORMAT_STR;
        case ERR_NFOUND:
            return ERR_NFOUND_STR;
        case ERR_SRVSIDE:
            return ERR_SRVSIDE_STR;
        case ERR_UNIMPL:
            return ERR_UNIMPL_STR;
        default :
            return "XXX";
    }
}

string get_code_dsc(int status_code) {
    switch(status_code) {
        case SUCCESS:
            return SUCCESS_DSC;
        case ERR_FMOVED:
            return ERR_FMOVED_DSC;
        case ERR_FORMAT:
            return ERR_FORMAT_DSC;
        case ERR_NFOUND:
            return ERR_NFOUND_DSC;
        case ERR_SRVSIDE:
            return ERR_SRVSIDE_DSC;
        case ERR_UNIMPL:
            return ERR_UNIMPL_DSC;
        default :
            return "xxxxx";
    }
}

string to_lowercase(string str) {
    transform(str.begin(), str.end(), str.begin(),
              [](unsigned char c){ return std::tolower(c); });
    return str;
}

bool search_double_endl(char* line) {
    string s(line);
    return regex_match(s, DOUBLE_ENDL_MATCH);
}

bool eol_is_carriage_return(char* line) {
    size_t len = strlen(line);
    return (line[len - 1] == '\r');
}

bool starts_with_endl(char* line) {
    return (line[0] == '\n');
}

bool is_prefix(char* cat_path, char* full_path) {
    string pre(cat_path);
    string full(full_path);
    auto mispair = mismatch(pre.begin(), pre.end(), full.begin());
    return (*mispair.first == '\0');
}

bool single_slash(char* s_arg) {
    string s(s_arg);
    return regex_match(s, SLASH_MATCH);
}

int match_method(char* s_arg, bool* get_flag, bool* head_flag) {
    string s(s_arg);
    *get_flag = regex_match(s, GET_MATCH);
    *head_flag = regex_match(s, HEAD_MATCH);
    if (!(*get_flag) && !(*head_flag))
        return ERR_FORMAT;
    return 0;
}

int match_header_field(char* str_arg, bool* conn_cl_flag, 
                       bool* cont_len_flag, bool* srv_flag) {
    string str(str_arg);
    if (regex_match(str, HEADER_EMPTY))
        return MESSAGE_OVER;

    str = to_lowercase(str);
    if (!regex_match(str, HEADER_MATCH))
        return ERR_FORMAT;

    bool h_conn_close = regex_match(str, HEADER_CONN_CLOSE_MATCH);
    bool h_cont_len = regex_match(str, HEADER_CONT_LEN_MATCH);
    bool correct_len = regex_match(str, HEADER_CONT_CORR_LEN_MATCH);
    bool h_server = regex_match(str, HEADER_SERVER_MATCH);

    if (h_server && (*srv_flag))
        return ERR_FORMAT; // repeated field-name
    else if (h_server)
        *srv_flag = true;

    if (h_conn_close && (*conn_cl_flag))
        return ERR_FORMAT; // repeated field-name
    else if (h_conn_close)
        *conn_cl_flag = true;

    if (h_cont_len && (*cont_len_flag))
        return ERR_FORMAT; // repeated field-name
    else if (h_cont_len && !correct_len)
        return ERR_FORMAT; // wrong length of content
    else if (h_cont_len)
        *cont_len_flag = true;
    
    return 0;
}

char* build_prebody_message(size_t len_arg) {
    string len = to_string(len_arg);
    string msg = HTTPV + SINGLE_SPACE + SUCCESS_STR + SINGLE_SPACE + 
                 SUCCESS_DSC + CRLF + 
                 HFIELD_SERVER + CRLF +
                 HFIELD_CONT_TYPE + CRLF +
                 HFIELD_NAME_CONT_L + COLON + SINGLE_SPACE + len + CRLF + CRLF;
    char *p = (char*)malloc((msg.size() + 1) * sizeof(char));
    strcpy(p, msg.c_str());
    return p;
}

char* build_err_message(int status_code) {
    string msg = HTTPV + SINGLE_SPACE + get_code_str(status_code) + SINGLE_SPACE + 
                 get_code_dsc(status_code) + CRLF +
                 HFIELD_SERVER + CRLF + CRLF;
    char *p = (char*)malloc((msg.size() + 1)* sizeof(char));
    strcpy(p, msg.c_str());
    return p;
}

char* build_location_message(char* name, char* ip, char* port) {
    string msg = HTTPV + SINGLE_SPACE + ERR_FMOVED_STR + SINGLE_SPACE + 
                 ERR_FMOVED_DSC + CRLF + 
                 HFIELD_SERVER + CRLF +
                 HFIELD_NAME_LOC + COLON + SINGLE_SPACE + PROT + string(ip) + COLON +
                 string(port) + string(name) + CRLF + CRLF;
    char *p = (char*)malloc((msg.size() + 1) * sizeof(char));
    strcpy(p, msg.c_str());
    return p;
}

char* connect_str(char* s1, char* s2) {    
    string str1(s1);
    string str2(s2);
    if (str1.size() + str2.size() >= MY_PATH_MAX)
        return NULL;
    string str = str1 + str2;
    char *p = (char*)malloc((str1.size() + str2.size() + 2) * sizeof(char));
    strcpy(p, str.c_str());
    return p;
}

char* filepath_from_token(char* line_arg) {
    if (line_arg == NULL)
        return NULL;

    string line(line_arg);
    size_t len = strlen(line_arg);
    string filename;

    if (line[0] == 'G') { // GET
        len -= TO_EXTRACT_FNAME_GET;
        filename = line.substr(4, len);
    }
    else { // HEAD
        len -= TO_EXTRACT_FNAME_HEAD;
        filename = line.substr(5, len);
    }
    char *p = (char*)malloc((filename.size() + 1) * sizeof(char));
    strcpy(p, filename.c_str());
    return p;
}
