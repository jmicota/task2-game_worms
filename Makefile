all: screen-worms-server screen-worms-client

screen-worms-server.o: screen-worms-server.cpp
	g++ -Wall -Wextra -O2 -std=c++17 screen-worms-server.cpp -c

screen-worms-client.o: screen-worms-client.cpp
	g++ -Wall -Wextra -O2 -std=c++17 screen-worms-client.cpp -c


screen-worms-server: screen-worms-server.o
	g++ -Wall -Wextra -O2 -std=c++17 screen-worms-server.o -o screen-worms-server

screen-worms-client: screen-worms-client.o
	g++ -Wall -Wextra -O2 -std=c++17 screen-worms-client.o -o screen-worms-client


clean:
	rm screen-worms-client.o screen-worms-client screen-worms-server.o screen-worms-server