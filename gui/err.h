#ifndef _ERR_
#define _ERR_

/* Wypisuje informacj�? o bL?�?dnym zakoL?czeniu funkcji systemowej
i koL?czy dziaL?anie programu. */
extern void syserr (const char *fmt, ...);

/* Wypisuje informacj�? o bL?�?dzie i koL?czy dziaL?anie programu. */
extern void fatal (const char *fmt, ...);

#endif